#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(dream-chpsy1-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./dream-chpsy1-sc-ioc-001.iocsh","IPADDR=172.30.44.00,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"DREAM-ChpSy1:")
epicsEnvSet(R1,"Chop-PSC-101:")
epicsEnvSet(R2,"Chop-PSC-102:")
epicsEnvSet(R3,"Chop-OC-103:")
epicsEnvSet(R4,"Chop-BC-101:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R1)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R2)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R3)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R4)")

iocInit()
#EOF
